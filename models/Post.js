const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const { getSlug, categories } = require("../lib/post");

const MAX_TITLE_CHARS = 100;
const MAX_DESCRIPTION_CHARS = 130;
const MAX_BODY_CHARS = 12000;

const postSchema = new Schema({
  authorId: { type: String, required: true },
  title: {
    type: String,
    required: true,
    validate: {
      validator: (value) => value.length <= MAX_TITLE_CHARS,
      message: (prop) => `Title must be <= ${MAX_TITLE_CHARS} characters.`,
    },
  },
  category: {
    type: String,
    required: true,
    validate: {
      validator: (value) =>
        categories.some((category) => category.title === value),
      message: (prop) =>
        `'${prop.value}' does not exist as a category. Nice try, you little hacker.`,
    },
  },
  description: {
    type: String,
    required: true,
    validate: {
      validator: (value) => value.length <= MAX_DESCRIPTION_CHARS,
      message: (prop) =>
        `Description must be <= ${MAX_DESCRIPTION_CHARS} characters.`,
    },
  },
  body: {
    type: String,
    required: true,
    validate: {
      validator: (value) => value.length <= MAX_BODY_CHARS,
      message: (prop) =>
        `Your post body must be <= ${MAX_BODY_CHARS} characters.`,
    },
  },
  thumbnailUrl: {
    type: String,
    required: true,
    validate: {
      validator: (value) => /(https?:\/\/.*\.(?:png|jpg|jpeg))/g.test(value),
      message: (prop) => `Thumbnail URL must end with a .jpg, .jpeg, or .png`,
    },
  },
  duplicateTitles: {
    type: Number,
    default: 0,
  },
  slug: {
    type: String,
    default: function () {
      return getSlug(this.title, this.duplicateTitles);
    },
  },
  dateCreated: { type: Number, default: () => Date.now() },
  dateEdited: { type: Number, default: () => Date.now() },
});

module.exports = mongoose.model("Post", postSchema);
