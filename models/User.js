const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// https://www.npmjs.com/package/mongoose-unique-validator
const uniqueValidator = require("mongoose-unique-validator");

const { isValidEmail } = require("../lib/auth");

const MAX_USERNAME_CHARS = 16;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async (value) => isValidEmail(value),
      message: (prop) => `Invalid email.`,
    },
  },
  username: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async (value) => value.length <= MAX_USERNAME_CHARS,
      message: (prop) =>
        `Username must be <= ${MAX_USERNAME_CHARS} characters.`,
    },
  },
  password: {
    type: String,
    required: true,
  },
  dateCreated: { type: Date, default: Date.now() },
  refreshToken: { type: String, required: true },
});

userSchema.plugin(uniqueValidator, {
  message: "{VALUE} is already in use",
});

module.exports = mongoose.model("User", userSchema);
