require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");

const PORT = process.env.PORT || "4000";
app.listen(PORT, () => console.log(`Listening on port ${PORT}`));

mongoose.connect(
  process.env.DB_CONNECT_URI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err) {
    if (err) {
      console.error("Error in connecting to DB.", err);
      return;
    }
    console.log("Connected to DB.");
  }
);

// Middleware
const cookieParser = require("cookie-parser");
app.use(
  cors({
    origin: process.env.CORS_ORIGIN,
    optionsSuccessStatus: 200,
    credentials: true,
    exposedHeaders: ["Set-Cookie"],
  })
);
app.use(express.static("static"));
app.use(cookieParser());
app.use(express.json());

// Routes
const authRoute = require("./routes/auth");
const postRoute = require("./routes/post");
app.use("/auth", authRoute);
app.use("/posts", postRoute);
