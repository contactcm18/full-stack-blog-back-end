const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const bcrypt = require("bcrypt");

const User = require("../models/User");
const getModelErrors = require("../lib/getModelErrors");
const { authenticated } = require("../middleware/auth");

const {
  createAccessToken,
  createRefreshToken,
  attachCookies,
  isValidEmail,
  sendPasswordResetEmail,
  hashPassword,
  isPasswordValid,
  minPasswordLength,
} = require("../lib/auth");

router.post("/register", async (req, res) => {
  const { email, username, password1, password2 } = req.body;

  const miscErrors = [];
  // Custom validation not used in Model
  if (!password1 || !password2 || password1 !== password2)
    miscErrors.push("Passwords do not match.");
  if (!isPasswordValid(password1))
    miscErrors.push(
      `Password must be at least ${minPasswordLength} characters in length.`
    );

  let hashedPassword = null;
  try {
    hashedPassword = await hashPassword(password1);
  } catch (error) {
    return res.sendStatus(400);
  }

  const user = new User({
    email,
    username,
    password: hashedPassword,
  });
  user.refreshToken = createRefreshToken(user);

  // Validate fields
  try {
    await user.validate();
  } catch (error) {
    return res
      .status(400)
      .json([...getModelErrors(error.errors), ...miscErrors]);
  }

  // If we caught any of our own errors, send them
  if (miscErrors.length) return res.status(400).json(miscErrors);

  user
    .save()
    .then(() => {
      attachCookies(res, createAccessToken(user), user.refreshToken);
      return res.json({ email, username: user.username });
    })
    .catch((error) => {
      return res
        .status(400)
        .json([...getModelErrors(error.errors), ...miscErrors]);
    });
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password)
    return res
      .status(400)
      .json([
        ...(!email ? ["Please enter an email."] : []),
        ...(!password ? ["Please enter a password."] : []),
      ]);

  if (!isValidEmail(email))
    return res.status(400).json(["Please enter a valid email."]);

  // Get user from DB
  const user = await User.findOne({ email });
  if (!user) return res.status(400).json(["User does not exist."]);

  // Compare passwords
  const match = await bcrypt.compare(password, user.password);
  if (!match) return res.status(400).json(["Incorrect password."]);

  const newRefreshToken = createRefreshToken(user);
  await user.updateOne({ $set: { refreshToken: newRefreshToken } });

  attachCookies(res, createAccessToken(user), newRefreshToken);

  return res.json({ email, username: user.username });
});

router.post("/isAuthenticated", authenticated, (req, res) => {
  return res.json(req.user);
});

router.post("/logout", (req, res) => {
  attachCookies(res, "", "");
  return res.sendStatus(200);
});

router.post("/updateUsername", authenticated, async (req, res) => {
  const { newUsername } = req.body;

  const newUserObject = {
    email: req.user.email,
    username: newUsername,
    _id: req.user._id,
  };
  const newRefreshToken = createRefreshToken(newUserObject);

  User.findOneAndUpdate(
    { _id: req.user._id },
    { $set: { username: newUsername, refreshToken: newRefreshToken } },
    { runValidators: true, context: "query", useFindAndModify: false }
  )
    .then((foundDocument) => {
      // Send updated tokens with username
      attachCookies(res, createAccessToken(newUserObject), newRefreshToken);
      return res.sendStatus(200);
    })
    .catch((error) => {
      return res.status(400).json(getModelErrors(error.errors));
    });
});

router.put("/reset-password", async (req, res) => {
  const { email } = req.body;
  if (!email) return res.sendStatus(400);

  const user = await User.findOne({ email });
  if (!user) return res.sendStatus(404);

  const jwtPasswordReset = jwt.sign(
    { currentPassword: user.password, userDateCreated: user.dateCreated },
    process.env.RESET_PASSWORD_SECRET
  );

  const response = await sendPasswordResetEmail(
    email,
    `${process.env.CLIENT_HOST}/reset-password/${jwtPasswordReset}`
  );

  return res.sendStatus(response.statusCode);
});

router.put("/reset-password/:jwtPasswordReset", async (req, res) => {
  const { password1, password2 } = req.body;
  if (!isPasswordValid(password1))
    return res
      .status(400)
      .json([
        `Password must be at least ${minPasswordLength} characters in length.`,
      ]);

  if (password1 !== password2)
    return res.status(400).json([`Passwords do not match.`]);

  const { jwtPasswordReset } = req.params;

  try {
    jwt.verify(jwtPasswordReset, process.env.RESET_PASSWORD_SECRET);
  } catch (error) {
    return res.sendStatus(403);
  }

  const { currentPassword, userDateCreated } = jwt.decode(jwtPasswordReset);
  const user = await User.findOne({
    password: currentPassword,
    dateCreated: userDateCreated,
  });
  if (!user) return res.sendStatus(404);

  try {
    hashedPassword = await hashPassword(password1);
    user.password = hashedPassword;
    user
      .save()
      .then(() => {
        return res.sendStatus(200);
      })
      .catch((error) => {
        return res.status(400).json(getModelErrors(error.errors));
      });
  } catch (error) {
    return res.sendStatus(400);
  }
});

router.delete("/deleteAccount", authenticated, (req, res) => {
  User.deleteOne({ email: req.user.email, username: req.user.username })
    .then(() => {
      attachCookies(res, "", "");
      return res.sendStatus(200);
    })
    .catch((error) => {
      return res.sendStatus(404);
    });
});

module.exports = router;
