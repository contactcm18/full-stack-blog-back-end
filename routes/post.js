const express = require("express");
const router = express.Router();
const xss = require("xss");

const Post = require("../models/Post");
const User = require("../models/User");

const getModelErrors = require("../lib/getModelErrors");
const { getSlug, categories } = require("../lib/post");

const { authenticated } = require("../middleware/auth");

router.get("/getPost", (req, res) => {
  const { category, slug } = req.query;
  if (!category || !slug) return res.sendStatus(400);

  Post.findOne({ category, slug })
    .then((document) => {
      User.findOne({ _id: document.authorId })
        .then((user) => {
          return res.json({ author: user.username, post: document });
        })
        .catch(() => {
          return res.sendStatus(404);
        });
    })
    .catch(() => {
      return res.sendStatus(404);
    });
});

router.get("/categories", (req, res) => {
  return res.json(categories);
});

router.post("/create", authenticated, async (req, res) => {
  const { title, category, description, body, thumbnailUrl } = req.body;

  // Does a post with "title" already exist?
  const duplicateTitles = await Post.find({ title });

  const filteredBody = xss(body);
  const filteredThumbnail = xss(thumbnailUrl);

  const post = new Post({
    authorId: req.user._id,
    title,
    category,
    description,
    body: filteredBody,
    thumbnailUrl: filteredThumbnail,
    duplicateTitles: duplicateTitles.length,
  });

  post
    .save()
    .then(() => {
      return res.json({ url: `/${post.category}/${post.slug}`, post });
    })
    .catch((error) => {
      return res.status(400).json(getModelErrors(error.errors));
    });
});

router.post("/update", authenticated, async (req, res) => {
  const { title, category, description, body, thumbnailUrl, slug } = req.body;

  const foundPost = await Post.findOne({ authorId: req.user._id, slug });
  if (!foundPost) return res.sendStatus(404);

  // Does a post with "title" already exist?
  const duplicateTitles = await Post.find({ title });

  const newSlug = getSlug(title, duplicateTitles.length);
  const filteredBody = xss(body);
  const filteredThumbnail = xss(thumbnailUrl);

  foundPost.slug = newSlug;
  foundPost.title = title;
  foundPost.category = category;
  foundPost.description = description;
  foundPost.body = filteredBody;
  foundPost.thumbnailUrl = filteredThumbnail;
  foundPost.duplicateTitles = duplicateTitles.length;

  try {
    await foundPost.save();
    return res.json({ slug: `/${category}/${newSlug}` });
  } catch (error) {
    return res.status(400).json(getModelErrors(error.errors));
  }
});

/**
 * Required query params:
 * order: "asc" || "desc"
 * startIndex: Number
 * endIndex: Number
 * category: String
 */
router.get("/getCategoryPosts", (req, res) => {
  let { order, startIndex, limit, category } = req.query;
  if (!order || !startIndex || !limit || !category) return res.sendStatus(400);

  if (!categories.some((cat) => cat.title === category))
    return res.sendStatus(400);

  Post.find({ category })
    .skip(parseInt(startIndex))
    .limit(parseInt(limit))
    .sort({ dateCreated: order })
    .then((documents) => {
      return res.json(documents);
    })
    .catch((error) => {
      console.log(error);
      return res.sendStatus(400);
    });
});

router.get("/getUserPosts", async (req, res) => {
  const { id } = req.query;
  if (!id) return res.sendStatus(400);

  const posts = await Post.find({ authorId: id });
  return res.json(posts);
});

router.delete("/delete", authenticated, async (req, res) => {
  const { slug } = req.query;
  if (!slug) return res.sendStatus(400);

  const { _id } = req.user;
  Post.deleteOne({ slug, authorId: _id }, (err) => {
    if (err) return res.sendStatus(400);
    return res.sendStatus(200);
  });
});

router.get("/getAllPosts", (req, res) => {
  Post.find({}, "category slug")
    .then((posts) => {
      return res.json(posts.map((post) => `/${post.category}/${post.slug}`));
    })
    .catch((error) => {
      return res.sendStatus(400);
    });
});

module.exports = router;
