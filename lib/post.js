exports.getSlug = (title, duplicateTitles) => {
  return `${title
    .toLowerCase()
    .replace(/[^\w ]+/g, "")
    .replace(/ +/g, "-")}${duplicateTitles ? `-${duplicateTitles + 1}` : ""}`;
};

exports.categories = [
  {
    title: "blog",
    description: "The generic category where you are likely to find anything.",
    imageUrl: "/images/categories/blog.jpg",
    url: "/blog",
  },
  {
    title: "tech",
    description: "Anything about technology.",
    imageUrl: "/images/categories/tech.jpg",
    url: "/tech",
  },
  {
    title: "sports",
    description: "",
    imageUrl: "/images/categories/sports.jpg",
    url: "/sports",
  },
  {
    title: "science",
    description: "",
    imageUrl: "/images/categories/science.jpg",
    url: "/science",
  },
  {
    title: "vehicles",
    description: "",
    imageUrl: "/images/categories/vehicles.jpg",
    url: "/vehicles",
  },
  {
    title: "nature",
    description: "",
    imageUrl: "/images/categories/nature.jpg",
    url: "/nature",
  },
];
