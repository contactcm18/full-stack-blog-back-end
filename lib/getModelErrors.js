/**
 * Returns array of error messages.
 * @param {*} errors The errors object, most likely error.errors.
 */
module.exports = (errors) => {
  let errorMessages = [];
  for (key in errors) {
    errorMessages.push(errors[key].message);
  }
  return errorMessages;
};
