const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");

const ACCESS_TOKEN_EXPIRE = "5m";
const REFRESH_TOKEN_EXPIRE = "7d";

exports.hashPassword = async (password) => {
  const salt = await bcrypt.genSalt();
  return new Promise(function (resolve, reject) {
    bcrypt
      .hash(password, salt)
      .then((data) => resolve(data))
      .catch(() => reject());
  });
};

exports.isPasswordValid = (password) =>
  password && password.length >= this.minPasswordLength;

exports.minPasswordLength = 8;

/**
 * Creates and returns a signed JWT.
 * @param {String} email
 * @param {String} username
 */
exports.createAccessToken = ({ email, username, _id }) =>
  jwt.sign({ email, username, _id }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: ACCESS_TOKEN_EXPIRE,
  });

/**
 * Creates and returns a signed JWT.
 * @param {String} email
 * @param {String} username
 */
exports.createRefreshToken = ({ email, username, _id }) =>
  jwt.sign({ email, username, _id }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: REFRESH_TOKEN_EXPIRE,
  });

/**
 * Attaches accessToken cookie and refreshToken cookie to res.
 * @param {Response<any, number>} res
 * @param {String} accessToken
 * @param {String} refreshToken
 */
exports.attachCookies = (res, accessToken, refreshToken) => {
  const cookieOptions = {
    domain: getDomain(),
    httpOnly: true,
    secure: enableSecureCookie(),
    sameSite: "Lax",
    maxAge: 24 * 60 * 60 * 1000,
  };
  res.cookie("accessToken", accessToken, cookieOptions);
  res.cookie("refreshToken", refreshToken, cookieOptions);
};

const getDomain = () =>
  process.env.HOST.includes("localhost")
    ? "localhost"
    : `.${process.env.HOST.match(/\w*\.\w*$/g)[0]}`;

const enableSecureCookie = () => process.env.HOST.includes("https");

exports.isValidEmail = (email) => /^\S+@\S+\.\S+$/g.test(email);

exports.sendPasswordResetEmail = async (email, resetLink) => {
  const transporter = nodemailer.createTransport({
    host: "smtp-relay.sendinblue.com",
    port: 587,
    secure: false,
    auth: {
      user: process.env.MAIL_OWNER,
      pass: process.env.MAIL_PASS,
    },
  });

  const verificationResult = await transporter.verify();
  if (!verificationResult)
    return {
      statusCode: 400,
      text: "Verification of transport failed.",
    };

  const mailOptions = {
    from: process.env.MAIL_FROM,
    to: email,
    subject: "Blogger Password Reset",
    text: `
      <p>If you did not request a password reset, you can safely ignore this email.</p>
      <a href="${resetLink}" target="_blank">Otherwise, reset password</a>
    `,
  };

  try {
    await transporter.sendMail(mailOptions);
  } catch (error) {
    return {
      statusCode: 400,
      text: "Send mail failed: " + error,
    };
  }

  return {
    statusCode: 200,
    text: "Mail successfully sent.",
  };
};
